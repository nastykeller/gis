import path from 'path';
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import eslintPlugin from 'vite-plugin-eslint';
// import vueI18n from '@intlify/vite-plugin-vue-i18n';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [
    vue(),
    eslintPlugin({ cache: false }),
    // vueI18n({
    //   include: path.resolve(__dirname, './path/to/src/locales/**'),
    // }),
  ], // TODO: check update for the cache in eslint plugin
  resolve: {
    alias: [
      {
        find: '@',
        replacement: path.resolve(__dirname, './src'),
      },
    ],
  },
  css: {
    preprocessorOptions: {
      scss: {
        additionalData: `
            @use "./src/assets/scss/variables.scss" as *;
            @use "./src/assets/scss/mixins/mixins.scss" as *;
            @use "./src/assets/scss/functions.scss" as *;
        `,
      },
    },
  },
  define: {
    'process.env': process.env,
  },
  build: {
    target: ['edge90', 'chrome90', 'firefox90', 'safari15'],
  },
});
