import * as R from 'ramda';

const isAcceptable = (mimetype: string): ((a: string[]) => boolean) => R.any(R.equals(mimetype));

const isVideo = (mimetype: string): boolean =>
  ['video/x-matroska', 'video/ogg', 'video/mp4', 'video/mpeg'].includes(mimetype);
const isZip = (mimetype: string): boolean =>
  [
    'zip',
    'application/octet-stream',
    'application/zip',
    'application/x-zip',
    'application/x-zip-compressed',
    'application/x-bzip2',
    'application/x-bzip',
  ].includes(mimetype);

const isPDF = (mimetype: string): boolean => ['application/pdf'].includes(mimetype);
const isImage = (mimetype: string): boolean =>
  [
    'image/apng',
    'image/png',
    'image/jpeg',
    'image/jpg',
    'image/svg+xml',
    'image/webp',
    'image/gif',
    'image/avif',
  ].includes(mimetype);

export { isAcceptable, isVideo, isZip, isPDF, isImage };
