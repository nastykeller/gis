// import { curry } from 'lodash';

// const childrenKey = 'children';

// export const filterTree = curry((childrenKey, textKey, array, searchText) => {
//   const search = searchText.toLowerCase();

//   const getNodes = (result, object) => {
//     if (object[textKey].toLowerCase().includes(search)) {
//       result.push(object);
//       return result;
//     }
//     if (Array.isArray(object[childrenKey])) {
//       const children = object[childrenKey].reduce(getNodes, []);
//       if (children.length) result.push({ ...object, [childrenKey]: children });
//     }
//     return result;
//   };

//   return array.reduce(getNodes, []);
// });

// // FIXME: update with Tree below
// export const updateTreeNode = curry((childrenKey, updatedNode, tree) => {
//   const getNodes = (result, node) => {
//     let newNode = {};
//     if (node.id === updatedNode.id) {
//       newNode = { ...updatedNode };
//     } else {
//       newNode = { ...node };
//     }
//     if (Array.isArray(node[childrenKey])) {
//       const children = node[childrenKey].reduce(getNodes, []);
//       if (children.length) {
//         newNode = { ...newNode, [childrenKey]: children };
//       }
//     }
//     result.push(newNode);
//     return result;
//   };

//   return tree.reduce(getNodes, []);
// });

// export const addTreeNode = curry((childrenKey, inserteddNode, parentId, tree) => {
//   const getNodes = (result, node) => {
//     let newNode = { ...node };

//     if (Array.isArray(node[childrenKey])) {
//       const children = node[childrenKey].reduce(getNodes, []);
//       if (children.length) {
//         if (newNode.id === parentId) {
//           newNode = { ...newNode, [childrenKey]: [...children, inserteddNode] };
//         } else {
//           newNode = { ...newNode, [childrenKey]: children };
//         }
//       }
//     }
//     result.push(newNode);
//     return result;
//   };

//   return tree.reduce(getNodes, []);
// });

// export const Tree = {
//   hasChildren: curry((key, node) => {
//     return typeof node === 'object' && typeof node[key] !== 'undefined' && node[key].length > 0;
//   }),

//   reduce: curry((reducerFn, init, node) => {
//     const acc = reducerFn(init, node);
//     if (!Tree.hasChildren(childrenKey, node)) {
//       return acc;
//     }
//     return node.children.reduce(Tree.reduce(reducerFn), acc);
//   }),

//   map: curry((mapFn, node) => {
//     const newNode = mapFn(node);
//     if (Tree.hasChildren(childrenKey, node)) {
//       return newNode;
//     }
//     newNode.children = node.children.map(Tree.map(mapFn));
//     return newNode;
//   }),
// };
// // TREE FUNCTIONS EXAMPLES

// export const flattenToArray = (arr, { children, ...data }) => {
//   return arr.concat([{ ...data }]);
// };

// export const flattenToSet = curry((key, setCollection, { children, ...data }) => {
//   return setCollection.add(data[key]);
// });

// // console.log(Tree.reduce(flattenToArray, [], menu));

// // function sumLinks(total, item) {
// //   return total + (item.type === 'link' ? 1 : 0);
// // }

// // console.log(Tree.reduce(sumLinks, 0, menu));

// // function addChildCount(node) {
// //   const countstr = (hasChildren(node)) ? ` (${node.children.length})` : '';
// //   return {
// //     ...node,
// //     text: node.text + countstr,
// //   }
// // }

// // console.log(Tree.map(addChildCount, menu));
