// import JsZip from 'jszip';
import * as zip from '@zip.js/zip.js';
// export { Tree } from './tree';

export const saveAs = (blob: Blob, filename: string): void => {
  const url = URL.createObjectURL(blob);

  const a: HTMLAnchorElement = document.createElement('a');
  a.style.display = 'none';

  a.href = url;
  a.download = filename || 'download';
  a.click();
  setTimeout(function () {
    window.URL.revokeObjectURL(url);
  }, 1000);
};

export const saveAsZip = async (blobs: { name: string; data: Blob }[]): Promise<void> => {
  const blobWriter = new zip.BlobWriter('application/zip');
  const writer = new zip.ZipWriter(blobWriter);

  await Promise.all(blobs.map(({ data, name }) => writer.add(name, new zip.BlobReader(data))));
  const data = await writer.close();
  const fileName = 'archive.zip';
  return saveAs(data, fileName);
};

export const randomInt = (min: number, max: number): number => {
  return Math.floor(Math.random() * (max - min + 1)) + min;
};

export const camelToSnakeCase = (str: string) => str.replace(/[A-Z]/g, letter => `_${letter.toLowerCase()}`);

export const objValToStr = (obj: Record<string, unknown>) => {
  return Object.keys(obj).reduce((acc, key) => {
    return { ...acc, [key]: String(obj[key]) };
  }, {});
};
