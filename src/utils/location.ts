import { GEOLOCATION_TIMEOUT } from '@/constants';

export const getPosition = (options?: PositionOptions): Promise<GeolocationPosition> => {
  return new Promise((resolve, reject) =>
    navigator.geolocation.getCurrentPosition(resolve, reject, { timeout: GEOLOCATION_TIMEOUT, ...options }),
  );
};

export const GeolocationCoordinatesSerializer = () => ({
  serializer: {
    read: (v: string) => (v ? JSON.parse(v) : null),
    write: (v: GeolocationCoordinates) =>
      JSON.stringify({
        latitude: v.latitude,
        longitude: v.longitude,
        altitude: v.altitude,
        altitudeAccuracy: v.altitudeAccuracy,
        accuracy: v.accuracy,
        speed: v.speed,
        heading: v.heading,
      }),
  },
});
