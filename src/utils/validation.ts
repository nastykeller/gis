const regexpLatitude = /^(\+|-)?(?:90(?:(?:\.0{1,4})?)|(?:[0-9]|[1-8][0-9])(?:(?:\.[0-9]{1,4})?))$/;
const regexpLongitude = /^(\+|-)?(?:180(?:(?:\.0{1,4})?)|(?:[0-9]|[1-9][0-9]|1[0-7][0-9])(?:(?:\.[0-9]{1,4})?))$/;

export function wrongLatitude(string: string): boolean {
  return regexpLatitude.test(string);
}
export function wrongLongitude(string: string): boolean {
  return regexpLongitude.test(string);
}
