import { defineStore } from 'pinia';
import { ref } from 'vue';

import { PROCESSES } from '@/constants';
import { report } from '@/services/api';
import { useWait } from '@/composable/useWait';

import { Report, ReportPreview, ReportSearchParams } from '@/types/Report';

const waiter = useWait();

const resetReports = (): GeoJSON.FeatureCollection<GeoJSON.Point, ReportPreview> => {
  return {
    type: 'FeatureCollection',
    features: [],
  };
};

const reports = ref(resetReports());
const currentReport = ref(null);
const currentReportCoordinates = ref({ lng: 0, lat: 0 });

export const useReportStore = defineStore('reportStore', () => {
  const setCurrentReportCoordinates = async (coordinates: { lng: number; lat: number }) => {
    currentReportCoordinates.value = {
      lng: Number(coordinates.lng.toFixed(4)),
      lat: Number(coordinates.lat.toFixed(4)),
    };
  };

  const fetchAll = waiter.waitFor(
    PROCESSES.REPORT.LOADING,
    async (params?: ReportSearchParams): Promise<GeoJSON.FeatureCollection<GeoJSON.Point, ReportPreview>> => {
      const result = await report.getAll(params);
      reports.value = result;
      return result;
    },
  );

  const fetchOne = waiter.waitFor(PROCESSES.REPORT.LOADING_ONE, async (id: number) => {
    return report.get(id);
  });

  const save = waiter.waitFor(PROCESSES.REPORT.SAVING, async (data: Report) => {
    const result = data?.id ? await report.update(data.id, data) : await report.create(data);
    return result;
  });

  return {
    reports,
    currentReport,
    fetchOne,
    fetchAll,
    save,
    setCurrentReportCoordinates,
    currentReportCoordinates,
  };
});
