import { defineStore } from 'pinia';
import { ref } from 'vue';
import { RouteLocationRaw } from 'vue-router';

export const useRouterStore = defineStore('routerStore', () => {
  const previousRoute = ref();
  const parentRoute = ref();

  const setPreviousRoute = (route: RouteLocationRaw) => (previousRoute.value = route);
  const setParentRoute = (route: RouteLocationRaw) => (parentRoute.value = route);

  return {
    previousRoute,
    setPreviousRoute,
    parentRoute,
    setParentRoute,
  };
});
