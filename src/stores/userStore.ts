import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import { useStorage } from '@vueuse/core';

import { auth } from '@/services/api';
import { apiConfig } from '@/config';
import { useWait } from '@/composable/useWait';

import { Profile, Roles, AuthProfile, Account } from '@/types/Account';
import { GEOLOCATION_ACCURACY, GEOLOCATION_TIMEOUT, GEOLOCATION_COORDINATES, PROCESSES } from '@/constants';
import { getPosition, GeolocationCoordinatesSerializer } from '@/utils/location';

const waiter = useWait();

const resetProfile = (): Profile => ({
  id: undefined,
  token: '',
  name: '',
  lastName: '',
  email: '',
  role: Roles.USER,
});

const resetGeoCoords = (): GeolocationCoordinates => GEOLOCATION_COORDINATES;

const currentGeoCoords = useStorage(
  'currentGeoCoords',
  resetGeoCoords(),
  undefined,
  GeolocationCoordinatesSerializer(),
);

try {
  currentGeoCoords.value = (await getPosition()).coords;
} catch {}

navigator.geolocation.watchPosition(
  val => {
    currentGeoCoords.value = val.coords;
  },
  undefined,
  {
    timeout: GEOLOCATION_TIMEOUT,
    enableHighAccuracy: GEOLOCATION_ACCURACY,
  },
);

const profile = useStorage(`${apiConfig.storageName}.profile`, resetProfile());
const token = useStorage(apiConfig.tokenStorageName, '');

const apiToken = computed(() => token.value);

const isAuthenticated = computed(() => token.value);
const timezone = ref(Intl.DateTimeFormat().resolvedOptions().timeZone);
const currentLanguage = ref('ru');

export const useUserStore = defineStore('userStore', () => {
  const login = waiter.waitFor(PROCESSES.ACCOUNT.LOADING, async (loginData: AuthProfile) => {
    profile.value = await auth.login(loginData);

    token.value = profile.value.token;

    return profile.value;
  });

  const verifyToken = async () => {
    const { valid } = await auth.verifyToken(profile.value.token);
    return valid;
  };

  const logout = () => {
    profile.value = resetProfile();
    token.value = null;

    // TODO: reset other stores here if needed
  };

  const signup = waiter.waitFor(PROCESSES.ACCOUNT.SAVING, async (data: AuthProfile) => {
    const result = await auth.signup(data);
    return result;
  });

  const updateProfile = waiter.waitFor(PROCESSES.ACCOUNT.SAVING, async (data: Partial<Account>) => {
    if (!profile.value.id) return;
    const result = await auth.update(profile.value.id, data);
    return result;
  });

  const setCurrentLanguage = (lang: string) => (currentLanguage.value = lang);

  return {
    apiToken,
    currentLanguage,
    setCurrentLanguage,
    profile,
    updateProfile,
    isAuthenticated,
    login,
    logout,
    signup,
    verifyToken,
    currentGeoCoords,
    timezone,
  };
});
