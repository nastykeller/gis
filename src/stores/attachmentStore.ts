import { defineStore } from 'pinia';
import { saveAs } from '@/utils';
import { useWait } from '@/composable/useWait';

import { PROCESSES } from '@/constants';
import { attachments } from '@/services/api';

const waiter = useWait();

// FIXME: think about vue-wait - adapt or change
export const useAttachmentStore = defineStore('attachment', () => {
  const fetchAll = waiter.waitFor(PROCESSES.ATTACHMENT.LOADING, async () => {
    const result = await attachments.getAll();
    // commit('SET_ALL', result);
    // if (Object.prototype.hasOwnProperty.call(params, 'page') && params.page > 0) {
    //   commit('ADD_FACILITIES', result);
    // } else {
    //   commit('SET_FACILITIES', result);
    // }

    return result;
  });

  const save = waiter.waitFor(PROCESSES.ATTACHMENT.SAVING, async (data: File) => {
    const formData = new FormData();
    formData.append('file', data);

    const result = await attachments.create(formData);
    return result;
  });

  const remove = waiter.waitFor(PROCESSES.ATTACHMENT.REMOVING, async (id: number) => {
    const result = await attachments.remove(id);
    return result;
  });

  const download = waiter.waitFor(PROCESSES.ATTACHMENT.LOADING, async (id: number, name: string) => {
    const result = await attachments.get(id);
    saveAs(result, name);
    return true;
  });

  // const downloadAll = async (attachmentList) => {
  //   const blobResults = await Promise.all(attachmentList.map(({ id }) => attachments.get(id)));

  //   saveAsZip(attachmentList.map(({ name }, i) => ({ name, data: blobResults[i] })));
  //   return true;
  // };

  return { fetchAll, save, download, remove };
});
