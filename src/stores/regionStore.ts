import { defineStore } from 'pinia';
import { ref, computed } from 'vue';
import { PROCESSES } from '@/constants';
import { region } from '@/services/api';
import { useWait } from '@/composable/useWait';
import { Region } from '@/types/Region';

const waiter = useWait();

const resetRegions = (): GeoJSON.FeatureCollection<GeoJSON.Polygon, Region> => {
  return {
    type: 'FeatureCollection',
    features: [],
  };
};

const regions = ref(resetRegions());
const regionsProps = computed(() => regions.value.features.map(x => x.properties));
const currentRegion = ref<GeoJSON.Feature<GeoJSON.Polygon, Region>>();

export const useRegionStore = defineStore('regionStore', () => {
  const fetchAll = waiter.waitFor(
    PROCESSES.REGION.LOADING,
    async (): Promise<GeoJSON.FeatureCollection<GeoJSON.Polygon>> => {
      const result = await region.getAll();
      regions.value = result;
      return result;
    },
  );

  const setCurrentRegion = async (region: Region) => {
    currentRegion.value = regions.value.features.find(x => region.id === x.properties.id);
  };

  return { regions, regionsProps, currentRegion, fetchAll, setCurrentRegion };
});
