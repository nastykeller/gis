import mapboxgl from 'mapbox-gl';
import { createApp, h } from 'vue';

import i18n from '@/services/i18n';
import router from '@/router';
import MapReportPopup from '@/components/map/MapReportPopup.vue';
import { ReportPreview, ReportTypes } from '@/types/Report';

import { useCharts } from '@/composable/useChartElement';
const { createDonutChartElement } = useCharts();

const reportPoints = new Map();
let reportPointsOnScreen = new Map();

// FIXME: make constant/ configurable
const reportType = {
  quickReport: '#2bb874',
  avalancheReport: '#7d50ff',
  snowpackReport: '#3890f8',
  weatherReport: '#ffa049',
  incidentReport: '#ff5367',
};

export default function useMapReportPoints(map: mapboxgl.Map) {
  // FIXME: REVIEW
  // FIXME: make universal function for points and clusters
  const drawReportPoints = (features: GeoJSON.Feature[]) => {
    const newReportPoints = new Map();

    features.forEach(f => {
      if (!f?.properties || f?.properties?.cluster) return;
      const props = f.properties;
      const coords = (f.geometry as GeoJSON.Point).coordinates as [number, number];

      const id = props.id;

      if (!reportPoints.has(id)) {
        const data = Object.keys(reportType)
          .filter(key => props[key])
          .map((key, index, arr) => ({
            percent: 100 / arr.length,
            color: reportType[key as ReportTypes],
          }));

        const chartDonut = createDonutChartElement(data);
        chartDonut.addEventListener('mouseenter', () => {
          chartDonut.style.cursor = 'pointer';
        });

        // TODO: is it optimal to register new app?
        const el = document.createElement('div');
        const markerPopup = createApp({
          // FIXME: destoroy after render!!!!
          setup() {
            return () => h(MapReportPopup, { report: props as ReportPreview });
          },
        })
          .use(i18n)
          .use(router)
          .mount(el);

        const popup = new mapboxgl.Popup({
          anchor: 'top-left',
          closeButton: false,
          closeOnClick: true,
          maxWidth: 'none',
          className: 'view-report-popup',
        }).setDOMContent(markerPopup.$el);

        chartDonut.addEventListener('click', e => {
          e.preventDefault();
        });
        const markerNew = new mapboxgl.Marker({
          element: chartDonut,
        })
          .setLngLat(coords)
          .setPopup(popup);

        reportPoints.set(id, markerNew);
        markerNew.getElement().addEventListener(
          'click',
          e => {
            marker.togglePopup();
            e.stopPropagation();
          },
          false,
        );
      }
      const marker = reportPoints.get(id);
      newReportPoints.set(id, marker);
      if (!reportPointsOnScreen.has(id)) {
        marker.addTo(map);
      }
    });

    // for every marker we've added previously, remove those that are no longer visible
    for (const id of reportPointsOnScreen.keys()) {
      if (!newReportPoints.has(id)) {
        reportPointsOnScreen.get(id).remove();
      }
    }
    reportPointsOnScreen = new Map(newReportPoints);
  };

  return {
    drawReportPoints,
  };
}
