import mapboxgl from 'mapbox-gl';
import { useUserStore } from '@/stores/userStore';
import mapLocationIcon from '@/assets/images/current_location.svg';

export default function useMapUserLocation(map: mapboxgl.Map) {
  const addUserLocationIconOnMap = () => {
    const userStore = useUserStore();

    const el = document.createElement('img');
    el.src = mapLocationIcon;

    new mapboxgl.Marker({
      element: el,
    })
      .setLngLat([userStore.currentGeoCoords.longitude, userStore.currentGeoCoords.latitude])
      .addTo(map);
  };

  return { addUserLocationIconOnMap };
}
