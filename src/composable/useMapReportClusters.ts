import mapboxgl from 'mapbox-gl';

import { ReportTypes } from '@/types/Report';

import { useCharts } from '@/composable/useChartElement';
const { createDonutChartElement } = useCharts();

const reportClusters = new Map();
let reportClustersOnScreen = new Map();

// FIXME: make constant/ configurable
const reportType = {
  quickReport: '#2bb874',
  avalancheReport: '#7d50ff',
  snowpackReport: '#3890f8',
  weatherReport: '#ffa049',
  incidentReport: '#ff5367',
};

export default function useMapReportPoints(map: mapboxgl.Map) {
  // FIXME: make universal function for points and clusters
  // FIXME: REVIEW - clusters don't refresh after page reload
  const drawReportClusters = (features: mapboxgl.MapboxGeoJSONFeature[]) => {
    const newReportClusters = new Map();
    // for every cluster on the screen, create an HTML marker for it (if we didn't yet),
    // and add it to the map if it's not there already
    features.forEach(f => {
      if (!f?.properties?.cluster) return;
      const props = f.properties;
      const coords = (f.geometry as GeoJSON.Point).coordinates as [number, number];

      const id = props.cluster_id;

      if (!reportClusters.has(id)) {
        const data = Object.keys(reportType)
          .filter(key => props[key])
          .map((key, index, arr) => ({
            percent: 100 / arr.length,
            color: reportType[key as ReportTypes],
          }));
        reportClusters.set(
          id,
          new mapboxgl.Marker({
            element: createDonutChartElement(data, `${props.point_count_abbreviated}`),
          }).setLngLat(coords),
        );
      }
      const marker = reportClusters.get(id);
      newReportClusters.set(id, marker);
      if (!reportClustersOnScreen.has(id)) {
        marker.addTo(map);
      }
    });

    // for every marker we've added previously, remove those that are no longer visible
    for (const id of reportClustersOnScreen.keys()) {
      if (!newReportClusters.has(id)) {
        reportClustersOnScreen.get(id).remove();
      }
    }
    reportClustersOnScreen = new Map(newReportClusters);
  };

  return {
    drawReportClusters,
  };
}
