import { useI18n } from 'vue-i18n';
import { useUserStore } from '@/stores/userStore';
import { ToastType, useToast } from '@/composable/useToast';

import { ApiError, UnauthorizedApiError } from '@/services/api/httpErrors';
import { ValidationError } from '@/errors';
import { ErrorObject } from '@vuelidate/core';

interface UsableErrorHandler {
  errorHandler: (error: Error) => boolean;
  hasErrors: (errors: ErrorObject[]) => boolean;
}

// TODO: status code 500 handler? hide error or show special page?
// TODO: 403
// TODO: 404
export const useErrorHandler = (): UsableErrorHandler => {
  const { toast } = useToast();
  const { te, t } = useI18n();
  const userStore = useUserStore();

  const errorHandler = (error: Error) => {
    if (error instanceof UnauthorizedApiError) {
      userStore.logout();
      return false;
    }
    if (error instanceof ApiError) {
      const { errors } = error;
      errors.forEach(message => {
        toast({
          type: ToastType.ERROR,
          message: message.detail,
        });
      });
    }
    if (error instanceof ValidationError) {
      error.errors.forEach(message => {
        if (te(message)) {
          toast({
            type: ToastType.ERROR,
            message: t(message),
          });
        } else {
          toast({
            type: ToastType.ERROR,
            message,
          });
        }
      });
    }
    console.log(error);
    return false;
  };

  const hasErrors = (errors: ErrorObject[]) => {
    return Array.isArray(errors) && Boolean(errors.length);
  };

  return { errorHandler, hasErrors };
};
