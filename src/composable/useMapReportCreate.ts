import mapboxgl from 'mapbox-gl';
import { createApp, h } from 'vue';
import i18n from '@/services/i18n';
import router from '@/router';

import BaseButton from '@/components/BaseButton.vue';

import { useReportStore } from '@/stores/reportStore';

const { t } = i18n.global;

let createReportMarker: mapboxgl.Marker;
let createReportPopup: mapboxgl.Popup;

export default function useMapReportCreate(map: mapboxgl.Map) {
  const reportStore = useReportStore();

  const reportCreate = () => {
    router.push({ name: 'report-create' });
  };
  // FIXME: REVIEW
  // TODO: think about clickoutside
  map.on('click', e => {
    if (map === undefined) return;

    const coordinates = { lng: e.lngLat.lng, lat: e.lngLat.lat };

    if (createReportMarker) {
      createReportMarker.remove();
    }

    const marker = new mapboxgl.Marker({
      color: 'red',
    })
      .setLngLat(coordinates)
      .addTo(map);
    createReportMarker = marker;

    // eslint-disable-next-line vue/one-component-per-file
    const createReportBtn = createApp({
      setup() {
        return () =>
          h(
            BaseButton,
            {
              type: 'float',
              kind: 'tonal',
              onClick: () => {
                createReportMarker.remove();
                createReportPopup.remove();
                reportStore.setCurrentReportCoordinates(coordinates);
                reportCreate();
              },
            },
            () => t('reports.create'),
          );
      },
    });

    const el = document.createElement('div');
    const mountedReportBtn = createReportBtn.mount(el);

    const popup = new mapboxgl.Popup({
      anchor: 'top-left',
      closeButton: false,
      closeOnClick: true,
      className: 'create-report-popup',
    })
      .setLngLat(coordinates)
      .setDOMContent(mountedReportBtn.$el)
      .addTo(map);

    createReportPopup = popup;
  });
}
