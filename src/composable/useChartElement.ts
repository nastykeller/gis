const createDonutChartElement = (segments: { percent: number; color: string }[], text?: string) => {
  const chart = document.createElement('div');
  const svg = document.createElementNS('http://www.w3.org/2000/svg', 'svg');
  const width = 16 + (text?.length ?? 0) * 14;
  const height = 16 + (text?.length ?? 0) * 14;
  svg.setAttribute('width', `${width}`);
  svg.setAttribute('height', `${height}`);
  svg.setAttribute('viewBox', `0 0 ${width} ${height}`);
  chart.appendChild(svg);

  const circleBackground = document.createElementNS('http://www.w3.org/2000/svg', 'circle');
  circleBackground.setAttribute('fill', 'white');
  circleBackground.setAttribute('r', `${width / 2}`);
  circleBackground.setAttribute('cx', `${width / 2}`);
  circleBackground.setAttribute('cy', `${height / 2}`);
  svg.appendChild(circleBackground);

  if (text) {
    const textElement = document.createElementNS('http://www.w3.org/2000/svg', 'text');

    textElement.setAttribute('font-size', '14');
    textElement.setAttribute('font-weight', 'bold');
    textElement.setAttribute('font-family', 'Inter');
    textElement.setAttribute('x', `${width / 2}`);
    textElement.setAttribute('y', `${(height + 1) / 2}`);
    textElement.setAttribute('text-anchor', 'middle');
    textElement.setAttribute('dominant-baseline', 'middle');

    textElement.textContent = text;

    svg.appendChild(textElement);
  }

  let previousDashOffset: number;
  let previousDashGap: number;
  segments.forEach(item => {
    const circle = document.createElementNS('http://www.w3.org/2000/svg', 'circle');

    const strokeWidth = 5;
    const radius = width / 2 - strokeWidth / 2;
    const cx = width / 2;
    const cy = height / 2;
    const circumference = 2 * Math.PI * radius;

    const dashArrayFill = (item.percent / 100) * circumference;
    const dashArrayGap = ((100 - item.percent) / 100) * circumference;
    const dashOffset =
      previousDashOffset === undefined
        ? (2 * Math.PI * radius) / 4
        : (previousDashGap + previousDashOffset) % circumference;
    previousDashOffset = dashOffset;
    previousDashGap = dashArrayGap;
    circle.setAttribute('r', `${radius}`);
    circle.setAttribute('cx', `${cx}`);
    circle.setAttribute('cy', `${cy}`);
    circle.setAttribute('fill', 'transparent');
    circle.setAttribute('stroke', item.color);
    circle.setAttribute('stroke-width', `${strokeWidth}`);
    circle.setAttribute('stroke-dasharray', `${dashArrayFill} ${dashArrayGap}`);
    circle.setAttribute('stroke-dashoffset', `${dashOffset} `);
    svg.appendChild(circle);
  });

  return chart;
};

export const useCharts = () => {
  return {
    createDonutChartElement,
  };
};

// Another one approach with svg Path - https://dev.to/mustapha/how-to-create-an-interactive-svg-donut-chart-using-angular-19eo
//
// Vue.component('donutChart', {
//   template: '#donutTemplate',
//   props: ["initialValues"],
//   data() {
//     return {
//       angleOffset: -90,
//       chartData: [],
//       colors: ["#6495ED", "goldenrod", "#cd5c5c", "thistle", "lightgray"],
//       cx: 80,
//       cy: 80,
//       radius: 60,
//       sortedValues: [],
//       strokeWidth: 30,
//     }
//   },
//   computed: {
//     // adjust the circumference to add small white gaps
//     adjustedCircumference() {
//       return this.circumference - 2
//     },
//     circumference() {
//       return 2 * Math.PI * this.radius
//     },
//     dataTotal() {
//       return this.sortedValues.reduce((acc, val) => acc + val)
//     },
//     calculateChartData() {
//       this.sortedValues.forEach((dataVal, index) => {
//         const { x, y } = this.calculateTextCoords(dataVal, this.angleOffset)
//         // start at -90deg so that the largest segment is perpendicular to top
//         const data = {
//           degrees: this.angleOffset,
//           textX: x,
//           textY: y
//         }
//         this.chartData.push(data)
//         this.angleOffset = this.dataPercentage(dataVal) * 360 + this.angleOffset
//       })
//     },
//     sortInitialValues() {
//       return this.sortedValues = this.initialValues.sort((a,b) => b-a)
//     }
//   },
//   methods: {
//     calculateStrokeDashOffset(dataVal, circumference) {
//       const strokeDiff = this.dataPercentage(dataVal) * circumference
//       return circumference - strokeDiff
//     },
//     calculateTextCoords(dataVal, angleOffset) {
//       // t must be radians
//       // x(t) = r cos(t) + j
//       // y(t) = r sin(t) + j

//       const angle = (this.dataPercentage(dataVal) * 360) / 2 + angleOffset
//       const radians = this.degreesToRadians(angle)

//       const textCoords = {
//         x: this.radius * Math.cos(radians) + this.cx,
//         y: this.radius * Math.sin(radians) + this.cy
//       }
//       return textCoords
//     },
//     degreesToRadians(angle) {
//       return angle * (Math.PI / 180)
//     },
//     dataPercentage(dataVal) {
//       return dataVal / this.dataTotal
//     },
//     percentageString(dataVal) {
//       return `${Math.round(this.dataPercentage(dataVal) * 100)}%`
//     },
//     returnCircleTransformValue(index) {
//       return `rotate(${this.chartData[index].degrees}, ${this.cx}, ${this.cy})`
//     },
//     segmentBigEnough(dataVal) {
//       return Math.round(this.dataPercentage(dataVal) * 100) > 5
//     }
//   },
//   mounted() {
//     this.sortInitialValues
//     this.calculateChartData
//   }
// })
// new Vue({
//   el: "#app",
//   data() {
//     return {
//       values: [230, 308, 520, 130, 200]
//     }
//   },
// });

// View Compiled
