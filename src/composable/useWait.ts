import { ref } from 'vue';

type AsyncFuncType<T extends unknown[], U> = (...args: T) => Promise<U>;
export interface UsableWait {
  is: (name: string | string[]) => boolean;
  start: (name: string) => void;
  end: (name: string) => void;
  any: () => void;
  clear: () => void;
  waitFor: <T extends unknown[], U>(waiter: string, func: AsyncFuncType<T, U>) => AsyncFuncType<T, U>;
}

const waiters = ref<string[]>([]);

const is = (waiter: string | string[]) => {
  if (Array.isArray(waiter)) {
    return waiter.every(name => waiters.value.includes(name));
  }
  return waiters.value.includes(waiter);
};

const start = (name: string) => {
  waiters.value = [...waiters.value, name];
  return waiters;
};

const end = (name: string) => {
  waiters.value = waiters.value.filter(x => x !== name);
  return waiters;
};

const any = () => {
  return waiters.value.length;
};

const clear = () => {
  waiters.value = [];
};

const waitFor = <T extends unknown[], U>(waiter: string, func: AsyncFuncType<T, U>): AsyncFuncType<T, U> => {
  if (typeof func !== 'function') {
    return func;
  }

  return async (...args) => {
    try {
      start(waiter);
      return await func(...args);
    } finally {
      end(waiter);
    }
  };
};

export const useWait = (): UsableWait => ({
  is,
  start,
  end,
  any,
  clear,
  waitFor,
});
