import mapboxgl from 'mapbox-gl';
import { Ref, watch } from 'vue';

import { useRegionStore } from '@/stores/regionStore';
import { MAP } from '@/constants';

// FIXME: make constants configurable
export default function useMapReportCreate(map: Ref<mapboxgl.Map | undefined>) {
  const regionStore = useRegionStore();

  watch(
    () => regionStore.currentRegion,
    value => {
      if (!value || !map.value) return;
      const coordinates = value.geometry.coordinates[0];
      const bounds = coordinates.reduce((bounds, coord) => {
        return bounds.extend(coord as mapboxgl.LngLatLike);
      }, new mapboxgl.LngLatBounds(coordinates[0] as mapboxgl.LngLatLike, coordinates[0] as mapboxgl.LngLatLike));

      map.value.fitBounds(bounds);
    },
    {
      deep: true,
    },
  );

  const toggleRegions = () => {
    if (!map.value) return;

    const visibility = map.value.getLayoutProperty(MAP.LAYER.REGIONS, 'visibility') === 'visible' ? 'none' : 'visible';

    map.value.setLayoutProperty(MAP.LAYER.REGIONS, 'visibility', visibility);
    map.value.setLayoutProperty('outline', 'visibility', visibility);
    map.value.setLayoutProperty(MAP.LAYER.REGIONS_NAMES, 'visibility', visibility);
  };

  const addRegionsOnMap = () => {
    regionStore.fetchAll().then(regions => {
      if (!map.value) return;

      map.value.addSource(MAP.SOURCE.REGIONS, {
        type: 'geojson',
        data: regions,
      });

      map.value.addLayer({
        id: MAP.LAYER.REGIONS,
        type: 'fill',
        source: MAP.SOURCE.REGIONS,
        layout: {
          visibility: 'none',
        },
        paint: {
          'fill-color': '#eee8ff',
          'fill-opacity': 0.25,
        },
      });

      map.value.addLayer({
        id: 'outline',
        type: 'line',
        source: MAP.SOURCE.REGIONS,
        layout: {
          visibility: 'none',
        },
        paint: {
          'line-color': '#8F68FF',
          'line-width': 2,
          'line-dasharray': [2, 1],
        },
      });

      map.value.addLayer({
        id: MAP.LAYER.REGIONS_NAMES,
        type: 'symbol',
        source: MAP.SOURCE.REGIONS,
        layout: {
          'text-field': ['get', 'title'],
          'text-font': ['Open Sans Semibold', 'Arial Unicode MS Bold'],
          'text-anchor': 'center',
          visibility: 'none',
        },
        paint: {
          'text-color': '#6E45E3',
        },
      });
    });
  };

  return { toggleRegions, addRegionsOnMap };
}
