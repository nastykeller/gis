export type DateRangeType = {
  start: Date | null;
  end: Date | null;
};
