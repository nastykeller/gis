import { SubsetOmit } from '@/types/utils';

export enum Roles {
  GUEST = 'USER',
  USER = 'USER',
  PRO = 'PRO',
  ADMIN = 'ADMIN',
}

export interface Account {
  id: number;
  name: string;
  lastName?: string;
  email: string;
  role: Roles;
}

export type Profile = SubsetOmit<Account, 'id'> & {
  token: string;
};

export interface AuthProfile {
  email: string;
  password: string;
  name?: string;
  rememberMe?: boolean;
}

export interface TokenVerification {
  valid: boolean;
}
