export interface AppErrorModel {
  errors: { fieldName: string; message: string }[];
}
