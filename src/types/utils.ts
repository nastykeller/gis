export type Mutable<T> = {
  -readonly [P in keyof T]: T[P];
};

export type ObjectEntries<T> = T extends ArrayLike<infer U>
  ? Array<[string, U]>
  : Array<{ [K in keyof T]: [K, T[K]] }[keyof T]>;

export type Modify<T, R> = Omit<T, keyof R> & R;

export type SubsetPick<T, U extends keyof T> = Pick<T, U> & Partial<T>;
export type SubsetOmit<T, U extends keyof T> = Omit<T, U> & Partial<T>;

export type Optional<T, K extends keyof T> = Pick<Partial<T>, K> & Omit<T, K>;
