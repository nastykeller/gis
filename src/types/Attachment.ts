export interface Attachment {
  id?: number;
  originalName: string;
  systemName?: string;
  mimetype: string;
  url: string;
}
