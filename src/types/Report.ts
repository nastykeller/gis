import { Attachment } from '@/types/Attachment';
import { Account } from './Account';

export enum RideQuality {
  WONDERFUL = 'WONDERFUL',
  GOOD = 'GOOD',
  NOT_BAD = 'NOT_BAD',
  BAD = 'BAD',
}

export enum SnowCondition {
  DEEP_AND_LOOSE = 'DEEP_AND_LOOSE',
  PUFFY = 'PUFFY',
  BLOWN = 'BLOWN',
  HEAVY = 'HEAVY',
  SOLID = 'SOLID',
  WET = 'WET',
  CRUST = 'CRUST',
}
export enum WeRode {
  GENTLE = 'GENTLE',
  CONVEX = 'CONVEX',
  SUNNY = 'SUNNY',
  FOREST_DENSE = 'FOREST_DENSE',
  FOREST_SPARSE = 'FOREST_SPARSE',
  STEEP = 'STEEP',
  ALPINE = 'ALPINE',
}
export enum WeStayedAwayFrom {
  GENTLE = 'GENTLE',
  CONVEX = 'CONVEX',
  SUNNY = 'SUNNY',
  FOREST_DENSE = 'FOREST_DENSE',
  FOREST_SPARSE = 'FOREST_SPARSE',
  STEEP = 'STEEP',
  ALPINE = 'ALPINE',
}
export enum DayWas {
  WARM = 'WARM',
  COLD = 'COLD',
  HAZY = 'HAZY',
  CLOUDY = 'CLOUDY',
  BLIZZARD = 'BLIZZARD',
  WINDY = 'WINDY',
  WET = 'WET',
  SUNNY = 'SUNNY',
}

export enum AvalancheCount {
  COUNT_1 = 'COUNT_1',
  COUNT_2_5 = 'COUNT_2_5',
  COUNT_6_10 = 'COUNT_6_10',
  COUNT_11_50 = 'COUNT_11_50',
}

export enum AvalancheSize {
  SIZE_1 = 'SIZE_1',
  SIZE_2 = 'SIZE_2',
  SIZE_3 = 'SIZE_3',
  SIZE_4 = 'SIZE_4',
  SIZE_5 = 'SIZE_5',
  SIZE_1_5 = 'SIZE_1_5',
  SIZE_2_5 = 'SIZE_2_5',
  SIZE_3_5 = 'SIZE_3_5',
  SIZE_4_5 = 'SIZE_4_5',
}

export enum AvalancheCondition {
  CM_30 = 'CM_30',
  SLAB = 'SLAB',
  WHUMPFING = 'WHUMPFING',
  TEMPERATURE = 'TEMPERATURE',
}

export enum AvalancheTime {
  LESS_12 = 'LESS_12',
  BETWEEN_12_AND_24 = 'BETWEEN_12_AND_24',
  BETWEEN_24_AND_48 = 'BETWEEN_24_AND_48',
  MORE_48 = 'MORE_48',
}

export enum AvalancheType {
  DRY_LOOSE = 'DRY_LOOSE',
  WET_LOOSE = 'WET_LOOSE',
  SNOWFALL_BOARD = 'SNOWFALL_BOARD',
  WIND_BOARD = 'WIND_BOARD',
  WET_BOARD = 'WET_BOARD',
  LONG_LIVED_BOARD = 'LONG_LIVED_BOARD',
  DEEP_LONG_LIVED_BOARD = 'DEEP_LONG_LIVED_BOARD',
  CORNICE = 'CORNICE',
  CORNICE_FROM_BOARD = 'CORNICE_FROM_BOARD',
}

export enum AvalancheTrigger {
  NATURAL = 'NATURAL',
  SKIER_OR_SNOWBOARDER = 'SKIER_OR_SNOWBOARDER',
  CLIMBER_OR_TOURIST = 'CLIMBER_OR_TOURIST',
  SNOWMOBILE_OR_SNOWBIKE = 'SNOWMOBILE_OR_SNOWBIKE',
  OTHER_VEHICLE = 'OTHER_VEHICLE',
  EXPLOSIVE = 'EXPLOSIVE',
}

export enum AvalancheTriggerSubType {
  ACCIDENTALLY = 'ACCIDENTALLY',
  PURPOSELY = 'PURPOSELY',
  REMOTELY = 'REMOTELY',
}

export enum CardinalPoint {
  NORTH = 'NORTH',
  SOUTH = 'SOUTH',
  WEST = 'WEST',
  EAST = 'EAST',
  NORTHEAST = 'NORTHEAST',
  NORTHWEST = 'NORTHWEST',
  SOUTHEAST = 'SOUTHEAST',
  SOUTHWEST = 'SOUTHWEST',
}

export enum ElevationBand {
  ALPINE = 'ALPINE',
  FOREST = 'FOREST',
  FOREST_BOUNDARY = 'FOREST_BOUNDARY',
}

export enum WeakLayerType {
  SUPERFICIAL_FROST = 'SUPERFICIAL_FROST',
  FACETS = 'FACETS',
  SUPERFICIAL_FROST_AND_FACETS = 'SUPERFICIAL_FROST_AND_FACETS',
  NEWLY_FALLEN_SNOW = 'NEWLY_FALLEN_SNOW',
  DEEP_FROS = 'DEEP_FROST',
}

export enum WindExposure {
  LEEWARD_SLOPE = 'LEEWARD_SLOPE',
  WINDWARD_SLOPE = 'WINDWARD_SLOPE',
  SIDE_LOADING_SLOPE = 'SIDE_LOADING_SLOPE',
  WIND_BLOWING_DONE_SLOPE = 'WIND_BLOWING_DONE_SLOPE',
  REVERSE_LOADING = 'REVERSE_LOADING',
  ABSENT = 'ABSENT',
}

export enum Vegetation {
  OPEN_SLOPE = 'OPEN_SLOPE',
  THICK_TREES = 'THICK_TREES',
  RARE_TREES = 'RARE_TREES',
}

export enum ObservationType {
  OBSERVATION_IS_SEPARATE_PLACE = 'OBSERVATION_IS_SEPARATE_PLACE',
  RESULT_OF_DA = 'RESULT_OF_DA',
}

export enum TestPitResult {
  NO_RESULTS = 'NO_RESULTS',
  VERY_EASY = 'VERY_EASY',
  EASY = 'EASY',
  MEDIUM = 'MEDIUM',
  HARD = 'HARD',
}

export enum CrackingPattern {
  SUDDENLY = 'SUDDENLY',
  WITH_RESIST = 'WITH_RESIST',
  JAGGED_CRACK = 'JAGGED_CRACK',
}
export enum SnowSurfaceCondition {
  NEWLY_FALLEN_SNOW = 'NEWLY_FALLEN_SNOW',
  CRUST_SNOW = 'CRUST_SNOW',
  FACETS = 'FACETS',
  SUPERFICIAL_FROST = 'SUPERFICIAL_FROST',
  SPRING_SNOW = 'SPRING_SNOW',
  CHANGING_SURFACE = 'CHANGING_SURFACE',
}

export enum TestFailureLayerCrystalType {
  NEWLY_FALLEN_SNOW = 'NEWLY_FALLEN_SNOW',
  SUPERFICIAL_FROST = 'SUPERFICIAL_FROST',
  FACETS = 'FACETS',
  DEEP_FROST = 'DEEP_FROST',
  CRUST_SNOW = 'CRUST_SNOW',
  OTHER = 'OTHER',
}

export enum SkyStatus {
  SUNNY = 'SUNNY',
  SLIGHTLY_CLOUDY = 'SLIGHTLY_CLOUDY',
  SCATTERED_CLOUDS = 'SCATTERED_CLOUDS',
  PARTLY_CLOUDY = 'PARTLY_CLOUDY',
  OVERCAST_CLOUDS = 'OVERCAST_CLOUDS',
  FOG = 'FOG',
}

export enum PrecipitationType {
  SNOW = 'SNOW',
  RAIN = 'RAIN',
  SNOW_WITH_RAIN = 'SNOW_WITH_RAIN',
  ABSENT = 'ABSENT',
}
export enum RainIntensity {
  DRIZZLE = 'DRIZZLE',
  LIGHT_RAIN = 'LIGHT_RAIN',
  MODERATE_RAIN = 'MODERATE_RAIN',
  HEAVY_RAIN = 'HEAVY_RAIN',
}
export enum TemperatureChange {
  DROP = 'DROP',
  STABLE = 'STABLE',
  RISE = 'RISE',
}
export enum WindSpeed {
  CALMLY = 'CALMLY',
  LIGHT = 'LIGHT',
  MODERATE = 'MODERATE',
  STRONG = 'STRONG',
  STORM = 'STORM',
}
export enum WindTransferSnow {
  ABSENT = 'ABSENT',
  LIGHT = 'LIGHT',
  MODERATE = 'MODERATE',
  STRONG = 'STRONG',
}

export enum ActivityType {
  ALPINISM = 'ALPINISM',
  HELISKI = 'HELISKI',
  SNOWMOBILE = 'SNOWMOBILE',
  SKI_TOUR = 'SKI_TOUR',
  HIKE = 'HIKE',
  FREERIDE = 'FREERIDE',
  OTHER = 'OTHER',
}

export enum Landform {
  CONVEX = 'CONVEX',
  CONCAVE = 'CONCAVE',
  FLAT = 'FLAT',
  WITHOUT_SUPPORT = 'WITHOUT_SUPPORT',
}
export enum SnowpackHeight {
  SHALLOW = 'SHALLOW',
  DEEP = 'DEEP',
  AVERAGE = 'AVERAGE',
  VARIABLE = 'VARIABLE',
}

export enum ReliefTrap {
  ABSENT = 'ABSENT',
  HOLLOW_TROUGH = 'HOLLOW_TROUGH',
  CLIFF = 'CLIFF',
  TREE = 'TREE',
  WATER = 'WATER',
  ABRUPT_TRANSITION_FROM_STEEP_TO_FLAT = 'ABRUPT_TRANSITION_FROM_STEEP_TO_FLAT',
}

export interface QuickReport {
  rideQuality?: RideQuality;
  snowCondition?: SnowCondition;
  weRode?: WeRode[];
  weStayedAwayFrom?: WeStayedAwayFrom[];
  dayWas?: DayWas[];
  avalancheCondition?: AvalancheCondition;
  comments?: string;
}

export interface AvalancheReport {
  date?: string;
  avalancheTime?: AvalancheTime;
  count?: AvalancheCount;
  size?: AvalancheSize;
  depth?: number;
  width?: number;
  length?: number;
  avalancheType?: AvalancheType[];
  avalancheTrigger?: AvalancheTrigger;
  avalancheTriggerSubType?: AvalancheTriggerSubType;
  distanceForRemoteTrigger?: number;
  startingAreaExposure?: CardinalPoint;
  elevationBand?: ElevationBand;
  startingAreaHeight?: number;
  startingAreaAngle?: number;
  offsetZoneHeight?: number;
  timeWeakLayer?: string;
  weakLayerType?: WeakLayerType[];
  crustNearWeakPoint?: boolean;
  windExposure?: WindExposure;
  vegetation?: Vegetation;
  comments?: string;
}
export interface SnowpackReport {
  observationType?: ObservationType;
  height?: number;
  elevationBand?: ElevationBand[];
  exposition?: CardinalPoint[];
  depth?: number;
  wumfing?: boolean;
  crackingSnow?: boolean;
  footprintDepth?: number;
  skiTrackDepth?: number;
  snowmobileTrackDepth?: number;
  testPitResult?: TestPitResult;
  crackingPattern?: CrackingPattern;
  depthDestruction?: number;
  snowSurfaceCondition?: SnowSurfaceCondition[];
  testFailureLayerCrystalType?: TestFailureLayerCrystalType[];
  comments?: string;
}
export interface WeatherReport {
  skyStatus?: SkyStatus;
  precipitationType?: PrecipitationType;
  snowfallIntensity?: number;
  rainIntensity?: RainIntensity;
  temperature?: number;
  minTemperature?: number;
  maxTemperature?: number;
  temperatureChange?: TemperatureChange;
  snowfallAmount?: number;
  snowfallRainAmount?: number;
  snowfallTotalAmount?: number;
  lastSnowfallDate?: string;
  windSpeed?: WindSpeed;
  directionWind?: CardinalPoint;
  windTransferSnow?: WindTransferSnow;
  comments?: string;
}

export interface IncidentReport {
  activityType?: ActivityType;
  anotherActivityType?: string;
  numberPeople?: number;
  completelyCovered?: number;
  partlyBuriedWithNormalBreathing?: number;
  partlyBuriedWithImpairedBreathing?: number;
  notCoveredAndWithoutInjury?: number;
  notCoveredAndInjured?: number;
  landform?: Landform;
  snowpackHeight?: SnowpackHeight;
  reliefTrap?: ReliefTrap[];
  comments?: string;
}

export enum ReportStatus {
  DRAFT = 'DRAFT',
  PUBLISHED = 'PUBLISHED',
}

export interface Report {
  id?: number;
  name: string;
  dateAndTime: string;
  latitude: number;
  longitude: number;
  comments?: string;
  images?: Attachment[];
  account?: Account;
  status: ReportStatus;

  quickReport?: QuickReport;
  avalancheReport?: AvalancheReport;
  snowpackReport?: SnowpackReport;
  weatherReport?: WeatherReport;
  incidentReport?: IncidentReport;
}

export interface ReportPreview {
  id: number;
  name: string;
  dateAndTime: string;
  latitude: number;
  longitude: number;
  comments?: string;
  account?: Account;
  status: ReportStatus;

  quickReport: boolean;
  avalancheReport: boolean;
  snowpackReport: boolean;
  weatherReport: boolean;
  incidentReport: boolean;
}

export type ReportSearchParams = {
  nameLike?: string;
  containAvalanche?: boolean;
  containIncident?: boolean;
  containShort?: boolean;
  containSnowpack?: boolean;
  containWeather?: boolean;
  showMyReports?: boolean;
  showFavorite?: boolean;
  status?: ReportStatus;
  boundingBox?: number[];
  page?: number;
  size?: number;
  sort?: string[];
};

export type ReportTypes = 'quickReport' | 'avalancheReport' | 'snowpackReport' | 'weatherReport' | 'incidentReport';

export enum ReportTypesEnum {
  quickReport = 'quickReport',
  avalancheReport = 'avalancheReport',
  snowpackReport = 'snowpackReport',
  weatherReport = 'weatherReport',
  incidentReport = 'incidentReport',
}

export type ReportFilterTypes = { [k in ReportTypesEnum]: boolean };
