export type Region = {
  id: number;
  title: string;
};
