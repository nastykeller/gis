import { Region } from '@/types/Region';
import http from '../http';

const modulePath = 'region';

const methods = {
  async getAll(): Promise<GeoJSON.FeatureCollection<GeoJSON.Polygon, Region>> {
    return (await http.get(modulePath)).json();
  },
};

export default methods;
