import http from '../http';
import { Attachment } from '@/types/Attachment';

const modulePath = 'attachment';

const methods = {
  async getAll(searchParams?: Record<string, string>): Promise<Attachment[]> {
    return (await http.get(modulePath, searchParams)).json();
  },

  async get(id: number): Promise<Blob> {
    return (await http.get(`${modulePath}/${id}`)).blob();
  },

  async remove(id: number): Promise<void> {
    await http.del(`${modulePath}/${id}`);
  },

  async create(data: FormData): Promise<Attachment> {
    return (await http.post(modulePath, data)).json();
  },
};

export default methods;
