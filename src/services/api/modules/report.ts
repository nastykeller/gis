import http from '../http';

import { Report, ReportPreview, ReportSearchParams } from '@/types/Report';
import { SubsetOmit } from '@/types/utils';
import { Attachment } from '@/types/Attachment';

const modulePath = 'report';

const methods = {
  async getAll(searchParams?: ReportSearchParams): Promise<GeoJSON.FeatureCollection<GeoJSON.Point, ReportPreview>> {
    return (await http.get(modulePath, searchParams)).json();
  },

  async get(id: number): Promise<Report> {
    return (await http.get(`${modulePath}/${id}`)).json();
  },

  async create(data: SubsetOmit<Report, 'id'>): Promise<Report> {
    return (await http.post(modulePath, data)).json();
  },

  async update(id: number, data: Partial<Report>): Promise<Report> {
    return (await http.patch(`${modulePath}/${id}`, data)).json();
  },

  async remove(id: number): Promise<void> {
    await http.del(`${modulePath}/${id}`);
  },

  async copy(id: number): Promise<Report> {
    return (await http.post(`${modulePath}/${id}`)).json();
  },

  async addAttachment(id: number, data: Attachment): Promise<Report> {
    return (await http.post(`${modulePath}/${id}`, data)).json();
  },
};

export default methods;
