import http from '../http';
import { Account, Profile, AuthProfile, TokenVerification } from '@/types/Account';
import { SubsetOmit } from '@/types/utils';

const modulePath = `account`;

const methods = {
  async login(data: AuthProfile): Promise<Profile> {
    return (await http.post(`${modulePath}/token`, data)).json();
  },

  async signup(data: AuthProfile): Promise<Account> {
    return (await http.post(`${modulePath}/signup`, data)).json();
  },

  async verifyToken(token: string): Promise<TokenVerification> {
    return (await http.get(`${modulePath}/token`, { token })).json();
  },

  async get(id: number): Promise<Account> {
    return (await http.get(`${modulePath}/${id}`)).json();
  },

  async getAll(searchParams: Record<string, string>): Promise<Account[]> {
    return (await http.get(modulePath, searchParams)).json();
  },

  async create(data: SubsetOmit<Account, 'id'>): Promise<Account> {
    return (await http.post(modulePath, data)).json();
  },

  async update(id: number, data: Partial<Account>): Promise<Account> {
    return (await http.patch(`${modulePath}/${id}`, data)).json();
  },
};

export default methods;
