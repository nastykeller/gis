// Partial JSON API implementation of RFC 7807
type APIErrorModel = {
  id?: number;
  code?: string;
  status?: string;
  title: string;
  detail: string;
  source?: {
    pointer: string;
    parameter: string;
  };
};

type APIErrorListModel = {
  errors: Array<APIErrorModel>;
};
export class ApiError extends Error {
  status: number;
  errors: APIErrorModel[];

  constructor(status = 400, { errors }: APIErrorListModel) {
    super('ApiError');
    this.name = 'ApiError';
    this.status = status;
    this.errors = errors;
  }
}

export class UnauthorizedApiError extends ApiError {
  constructor(errors: APIErrorListModel) {
    super(401, errors);
    this.name = 'UnauthorizedApiError';
  }
}

export class PermissionDeniedApiError extends ApiError {
  constructor(errors: APIErrorListModel) {
    super(403, errors);
    this.name = 'PermissionDeniedApiError';
  }
}

export class NotFoundApiError extends ApiError {
  constructor(errors: APIErrorListModel) {
    super(404, errors);
    this.name = 'NotFoundApiError';
  }
}

// TODO: type === 'json' parse Error and response.status === 204
export const handleResponseErrors = async (response: Response): Promise<void> => {
  const defaultError = { errors: [{ title: response.statusText, detail: response.statusText }] };
  const error: APIErrorListModel = await response.json().catch(() => defaultError);

  if (response.status === 401) {
    return Promise.reject(new UnauthorizedApiError(error));
  }

  if (!response.ok) {
    if (response.status >= 500) {
      return Promise.reject(new ApiError(response.status, defaultError));
    } else {
      return Promise.reject(new ApiError(response.status, error));
    }
  }
};
