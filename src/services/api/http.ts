import { objValToStr } from '@/utils';
import * as R from 'ramda';

import { handleResponseErrors } from './httpErrors';

let APIBaseUrl = '';
let APIToken = '';
let APILang = '';

export const setupAPI = (baseUrl: string, token: string, lang: string): void => {
  APIBaseUrl = baseUrl;
  APIToken = token;
  APILang = lang;
};

export const headersDefault = (): HeadersInit => {
  return {
    Accept: 'application/json, application/xml, text/plain, text/html, *.*',
    'Accept-Language': APILang,
    ...(APIToken ? { Authorization: `Bearer ${APIToken}` } : {}),
  };
};

const request = async (
  path: string,
  config: RequestInit,
  searchParams?: Record<string, unknown>,
): Promise<Response> => {
  const url = new URL(`${APIBaseUrl}/${path}`);

  if (searchParams) {
    url.search = new URLSearchParams(
      R.compose(objValToStr, R.reject(R.anyPass([R.isEmpty, R.isNil])))(searchParams),
    ).toString();
  }
  const request = new Request(url.toString(), { ...headersDefault(), ...config });
  const response = await fetch(request);

  await handleResponseErrors(response.clone());
  return response;
};

const parseBody = <T>(body: T, config?: RequestInit): RequestInit => {
  if (
    body instanceof FormData ||
    body instanceof Blob ||
    body instanceof URLSearchParams ||
    body instanceof ArrayBuffer ||
    typeof body === 'string'
  ) {
    return { ...config, body };
  }
  return {
    ...config,
    body: JSON.stringify(body),
    headers: {
      ...config?.headers,
      'Content-Type': 'application/json;charset=utf-8',
    },
  };
};

export function get(path: string, searchParams?: Record<string, unknown>, config?: RequestInit): Promise<Response> {
  return request(path, { method: 'get', ...config }, searchParams);
}

export function post<T = unknown>(path: string, body?: T, config?: RequestInit): Promise<Response> {
  return request(path, { method: 'post', ...parseBody(body, config) });
}

export function put<T = unknown>(path: string, body: T, config?: RequestInit): Promise<Response> {
  return request(path, { method: 'put', ...parseBody(body, config) });
}

export function patch<T = unknown>(path: string, body: T, config?: RequestInit): Promise<Response> {
  return request(path, { method: 'patch', ...parseBody(body, config) });
}

export function del(path: string, config?: RequestInit): Promise<Response> {
  return request(path, { method: 'delete', ...config });
}

// import { ObjectEntries } from '@/types/utils';
// export interface ResponsePromise extends Promise<Response> {
//   arrayBuffer: () => Promise<ArrayBuffer>;

//   blob: () => Promise<Blob>;

//   formData: () => Promise<FormData>;

//   json: <T = unknown>() => Promise<T>;

//   text: () => Promise<string>;
// }

// const responseTypes = {
//   json: 'application/json',
//   text: 'text/*',
//   formData: 'multipart/form-data',
//   arrayBuffer: '*/*',
//   blob: '*/*',
// } as const;

// export const handleJson = async <T = unknown>(response: Promise<Response>): Promise<T> => {
//   return (await response).json();
// };

export default {
  get,
  post,
  put,
  patch,
  del,
};
