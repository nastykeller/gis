/* eslint-disable import/prefer-default-export */
import { AbilityBuilder, Ability, AnyAbility } from '@casl/ability';
import { ROLE } from '@/constants';

export const ACTIONS = {
  MANAGE: 'manage',
  CREATE: 'create',
  READ: 'read',
  UPDATE: 'update',
  DELETE: 'delete',
};

export const SUBJECTS = {
  REPORT: 'report',
};

export const ability = new Ability();

// FIXME: a lot of rows, need to think how to make more readable and maintainable
export const defineAbilityFor = (role: string): AnyAbility => {
  const { can, build } = new AbilityBuilder(Ability);

  can([ACTIONS.READ], SUBJECTS.REPORT);

  switch (role) {
    case ROLE.ADMIN:
      can([ACTIONS.READ, ACTIONS.CREATE, ACTIONS.UPDATE], SUBJECTS.REPORT);
      break;
    case ROLE.PROFI:
      can([ACTIONS.READ, ACTIONS.CREATE, ACTIONS.UPDATE], SUBJECTS.REPORT);
      break;
    case ROLE.USER:
      can([ACTIONS.READ, ACTIONS.CREATE, ACTIONS.UPDATE], SUBJECTS.REPORT);
      break;
    default:
  }

  return build();
};

export default { ACTIONS, SUBJECTS, defineAbilityFor, ability };
