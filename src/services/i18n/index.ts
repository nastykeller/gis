import { createI18n } from 'vue-i18n';

import localeRu from './ru.json';

// import config from '../config';

const locale = 'ru'; // JSON.parse(localStorage.getItem(config?.storageName))?.language ?? 'en';

const i18n = createI18n({
  locale,
  fallbackLocale: 'ru',
  messages: {
    ru: localeRu,
  },
});

export const locales = ['ru'];

export default i18n;
