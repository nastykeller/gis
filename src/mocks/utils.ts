import config from '@/config';

import { DefaultRequestBody, MockedRequest, RestHandler } from 'msw';
const include = config.mockAPIInclude;
const exclude = config.mockAPIExclude;

export const filterHandlers = (handlers: {
  [key: string]: RestHandler<MockedRequest<DefaultRequestBody>>;
}): RestHandler<MockedRequest<DefaultRequestBody>>[] => {
  if (include) {
    return Object.keys(handlers)
      .filter(x => include.includes(x))
      .map(x => handlers[x as keyof typeof handlers]);
  }
  return Object.keys(handlers)
    .filter(x => !exclude?.includes(x))
    .map(x => handlers[x as keyof typeof handlers]);
};
