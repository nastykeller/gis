import { rest } from 'msw';
import config from '@/config';
import { Profile, Roles } from '@/types/Account';
import { randomInt } from '@/utils';
import { filterHandlers } from '../utils';

const baseUrl = config.apiConfig.baseUrl;

export const accountHandlers = filterHandlers({
  login: rest.post(`${baseUrl}/account/token`, (req, res, ctx) => {
    const profile: Profile = {
      id: randomInt(0, 1000),
      token: 'testtoken',
      name: 'test name',
      lastName: 'test last name',
      email: 'test@mail.ru',
      role: Roles.USER,
    };
    localStorage.setItem('token', profile.token);
    return res(ctx.status(200), ctx.delay(50), ctx.json(profile));
  }),

  validateToken: rest.get(`${baseUrl}/account/token`, (req, res, ctx) => {
    const token = localStorage.getItem('token');

    if (!token) {
      return res(
        ctx.status(200),
        ctx.json({
          valid: false,
        }),
      );
    }
    return res(ctx.json({ valid: true }));
  }),

  accountGet: rest.get(`${baseUrl}/account`, (req, res, ctx) => {
    const profile = {
      name: 'test name',
      lastName: 'test last name',
      email: 'test@mail.ru',
      role: Roles.USER,
    };
    return res(ctx.status(200), ctx.json(profile));
  }),

  accountCreate: rest.post(`${baseUrl}/account`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),

  accountUpdate: rest.patch(`${baseUrl}/account/:id`, (req, res, ctx) => {
    const profile = {
      name: 'test name',
      lastName: 'test last name',
      email: 'test@mail.ru',
      role: Roles.USER,
    };
    return res(ctx.status(200), ctx.json(profile));
  }),

  accountRegister: rest.post(`${baseUrl}/account/signup`, (req, res, ctx) => {
    const profile = {
      id: randomInt(0, 1000),
      name: 'test name',
      email: 'test@mail.ru',
      role: Roles.USER,
    };
    return res(ctx.status(200), ctx.delay(50), ctx.json(profile));
  }),
});
