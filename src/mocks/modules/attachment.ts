import { rest } from 'msw';
import config from '@/config';
import { Attachment } from '@/types/Attachment';
import { randomInt } from '@/utils';

const baseUrl = config.apiConfig.baseUrl;

export const attachmentHandlers = [
  rest.get(`${baseUrl}/attachment`, (req, res, ctx) => {
    return res(ctx.status(200));
  }),

  rest.get(`${baseUrl}/attachment`, (req, res, ctx) => {
    //   res(ctx.json(reportAll));
    // }),)];
    return res(ctx.status(200));
  }),

  rest.post<{ file: File }>(`${baseUrl}/attachment`, (req, res, ctx) => {
    const file = req.body.file;
    const attachment: Attachment = {
      id: randomInt(0, 100),
      originalName: file.name,
      systemName: 'system_filename_1',
      mimetype: 'image/jpg',
      url: 'https://picsum.photos/400/280',
    };

    return res(ctx.delay(), ctx.status(200), ctx.json(attachment));
  }),
  rest.delete(`${baseUrl}/attachment/:id`, (req, res, ctx) => {
    return res(ctx.delay(), ctx.status(200));
  }),
];
