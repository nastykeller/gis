import { rest } from 'msw';
import config from '@/config';
import { Report, ReportPreview, ReportStatus } from '@/types/Report';
import { Roles } from '@/types/Account';
import { getPosition } from '@/utils/location';
import { mockReport } from '../data/reportData';
import { filterHandlers } from '../utils';

const baseUrl = config.apiConfig.baseUrl;

let coordinates: GeolocationCoordinates = {
  latitude: 0,
  longitude: 0,
  altitude: 0,
  altitudeAccuracy: 0,
  accuracy: 0,
  speed: 0,
  heading: 0,
};

export const reportHandlers = filterHandlers({
  getReports: rest.get(`${baseUrl}/report`, async (req, res, ctx) => {
    try {
      const { coords } = await getPosition({ maximumAge: Infinity });
      coordinates = coords;
    } catch (e) {
      console.log('geo is failed');
      console.log(e);
    }

    const reports: GeoJSON.FeatureCollection<GeoJSON.Point, ReportPreview> = {
      type: 'FeatureCollection',
      features: [...Array(100).keys()].map(id => ({
        type: 'Feature',
        geometry: {
          type: 'Point',
          coordinates: [
            coordinates.longitude + Math.random() * 0.1 - Math.random() * 0.1,
            coordinates.latitude + Math.random() * 0.1 - Math.random() * 0.1,
          ],
        },
        properties: {
          id,
          name: `Лавинный отчет тестовый # ${id}`,
          dateAndTime: '2021-12-05T16:33:48.519Z',
          latitude: coordinates.latitude,
          longitude: coordinates.longitude,
          comments: 'Комментарий к отчету',
          account: {
            id,
            name: 'Минцев Григорий',
            email: 'test@email.com',
            role: Roles.PRO,
          },
          status: ReportStatus.PUBLISHED,

          quickReport: Math.random() < 0.5,
          avalancheReport: Math.random() < 0.5,
          snowpackReport: Math.random() < 0.5,
          weatherReport: Math.random() < 0.5,
          incidentReport: Math.random() < 0.5,
        },
      })),
    };

    return res(ctx.status(200), ctx.delay(500), ctx.json(reports));
  }),

  getReport: rest.get(`${baseUrl}/report/:id`, (req, res, ctx) => {
    const report: Report = mockReport;
    return res(ctx.status(200), ctx.delay(500), ctx.json(report));
  }),

  createReport: rest.post(`${baseUrl}/report`, (req, res, ctx) => {
    //   res(ctx.json(reportAll));
    // }),)];
    // return res(ctx.status(200));
    const report: Report = mockReport;
    return res(ctx.status(200), ctx.delay(500), ctx.json(report));
  }),
  updateReport: rest.patch(`${baseUrl}/report/:id`, (req, res, ctx) => {
    const report: Report = mockReport;
    return res(ctx.status(200), ctx.delay(500), ctx.json(report));
  }),
});
