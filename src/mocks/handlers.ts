import { accountHandlers } from './modules/account';
import { reportHandlers } from './modules/report';
import { attachmentHandlers } from './modules/attachment';
import { regionHandlers } from './modules/region';

export default [...accountHandlers, ...reportHandlers, ...attachmentHandlers, ...regionHandlers];
