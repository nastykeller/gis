import { setupWorker } from 'msw';
import handlers from './handlers';

const worker = setupWorker(...handlers);

export const startMockAPI = async (): Promise<void> => {
  await worker.start({
    onUnhandledRequest: 'bypass',
    quiet: true,
  });
};
