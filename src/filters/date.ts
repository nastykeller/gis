import { format } from 'date-fns';
import russianLocale from 'date-fns/locale/ru';

// TODO: review params and types
const formatDate = (date: string | Date, fmt = 'yyyy-MM-dd') => {
  try {
    return date ? format(new Date(date), fmt, { locale: russianLocale }) : date;
  } catch {
    return date;
  }
};

export { formatDate };
