import { ReportTypes } from './types/Report';

export const enum ROLE {
  ADMIN = 'ADMIN',
  USER = 'USER',
  PROFI = 'PROFI',
}

export const SORT_ORDER = {
  [-1]: 'DESC',
  0: 'NONE',
  1: 'ASC',
};

export const enum FILE_STATUS {
  DRAFT = 'DRAFT',
  UPLOADING = 'UPLOADING',
  COMPLETE = 'COMPLETE',
}

export const REPORT_STATUS = {
  DRAFT: 'DRAFT',
  IN_PROGRESS: 'IN_PROGRESS',
  COMPLETE: 'COMPLETE',
};

export const PROCESSES = {
  REPORT: {
    SAVING: 'report.saving',
    LOADING: 'report.loading',
    LOADING_ONE: 'report.loading_one',
    COPYING: 'report.copying',
    REMOVING: 'report.removing',
  },
  REGION: {
    SAVING: 'region.saving',
    LOADING: 'region.loading',
    LOADING_ONE: 'region.loading_one',
  },
  ACCOUNT: {
    SAVING: 'account.saving',
    LOADING: 'account.loading',
    REMOVING: 'account.removing',
  },
  ATTACHMENT: {
    SAVING: 'attachment.saving',
    LOADING: 'attachment.loading',
    REMOVING: 'attachment.removing',
  },
};

export const SEARCH_THRESHOLD = 3;

export const MAP = {
  ZOOM: 12,
  LAYER: {
    REPORTS: 'reports',
    REPORTS_CLUSTER: 'reports_cluster',
    REPORTS_CLUSTER_TEXT: 'reports_cluster_text',
    REGIONS: 'regions',
    REGIONS_NAMES: 'regions_names',
  },
  SOURCE: {
    REPORTS: 'reports',
    REGIONS: 'regions',
  },
  CLUSTER: {
    RADIUS: 100, // 0..512
    ZOOM: 14,
  },
};

export const GEOLOCATION_COORDINATES = {
  latitude: 43.3499,
  longitude: 42.4453,
  altitude: null,
  altitudeAccuracy: null,
  accuracy: 20,
  speed: null,
  heading: null,
};

export const GEOLOCATION_TIMEOUT = 3000;
export const GEOLOCATION_ACCURACY = true;

export const REPORT_OPTIONS_PREFIX = 'reports.options';

// FIXME: can we simplify work with translations?
export const REPORT_OPTIONS_TRANSLATE_KEYS: { [key in ReportTypes]: Record<string, string> } = {
  quickReport: {
    rideQuality: `${REPORT_OPTIONS_PREFIX}.ride_quality`,
    snowCondition: `${REPORT_OPTIONS_PREFIX}.snow_condition`,
    weRode: `${REPORT_OPTIONS_PREFIX}.we_rode`,
    weStayedAwayFrom: `${REPORT_OPTIONS_PREFIX}.we_stayed_away_from`,
    dayWas: `${REPORT_OPTIONS_PREFIX}.day_was`,
    avalancheCondition: `${REPORT_OPTIONS_PREFIX}.avalanche_condition`,
  },
  avalancheReport: {
    avalancheTime: `${REPORT_OPTIONS_PREFIX}.avalanche_time`,
    count: `${REPORT_OPTIONS_PREFIX}.avalanche_count`,
    size: `${REPORT_OPTIONS_PREFIX}.avalanche_size`,
    avalancheType: `${REPORT_OPTIONS_PREFIX}.avalanche_type`,
    avalancheTrigger: `${REPORT_OPTIONS_PREFIX}.avalanche_trigger`,
    avalancheTriggerSubType: `${REPORT_OPTIONS_PREFIX}.avalanche_trigger_subtype`,
    startingAreaExposure: `${REPORT_OPTIONS_PREFIX}.cardinal_point`,
    elevationBand: `${REPORT_OPTIONS_PREFIX}.elevation_band`,
    weakLayerType: `${REPORT_OPTIONS_PREFIX}.weak_layer_type`,
    windExposure: `${REPORT_OPTIONS_PREFIX}.wind_exposure`,
    vegetation: `${REPORT_OPTIONS_PREFIX}.vegetation`,
  },
  snowpackReport: {
    observationType: `${REPORT_OPTIONS_PREFIX}.observation_type`,
    elevationBand: `${REPORT_OPTIONS_PREFIX}.elevation_band`,
    exposition: `${REPORT_OPTIONS_PREFIX}.cardinal_point`,
    testPitResult: `${REPORT_OPTIONS_PREFIX}.test_pit_result`,
    crackingPattern: `${REPORT_OPTIONS_PREFIX}.cracking_pattern`,
    snowSurfaceCondition: `${REPORT_OPTIONS_PREFIX}.snow_surface_condition`,
    testFailureLayerCrystalType: `${REPORT_OPTIONS_PREFIX}.test_failure_layer_crystal_type`,
  },
  weatherReport: {
    skyStatus: `${REPORT_OPTIONS_PREFIX}.sky_status`,
    precipitationType: `${REPORT_OPTIONS_PREFIX}.precipitation_type`,
    rainIntensity: `${REPORT_OPTIONS_PREFIX}.rain_intensity`,
    temperatureChange: `${REPORT_OPTIONS_PREFIX}.temperature_change`,
    windSpeed: `${REPORT_OPTIONS_PREFIX}.wind_speed`,
    directionWind: `${REPORT_OPTIONS_PREFIX}.cardinal_point`,
    windTransferSnow: `${REPORT_OPTIONS_PREFIX}.wind_transfer_snow`,
  },
  incidentReport: {
    activityType: `${REPORT_OPTIONS_PREFIX}.activity_type`,
    landform: `${REPORT_OPTIONS_PREFIX}.landform`,
    snowpackHeight: `${REPORT_OPTIONS_PREFIX}.snowpack_height`,
    reliefTrap: `${REPORT_OPTIONS_PREFIX}.relief_trap`,
  },
};
