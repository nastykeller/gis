import pkg from '../package.json';

export const mockAPIEnable = import.meta.env.VITE_MOCK_API_ENABLE && import.meta.env.MODE !== 'production';
export const mockAPIInclude =
  import.meta.env.VITE_MOCK_API_INCLUDE &&
  typeof import.meta.env.VITE_MOCK_API_INCLUDE === 'string' &&
  import.meta.env.MODE !== 'production'
    ? import.meta.env.VITE_MOCK_API_INCLUDE.split(' ')
    : undefined;

export const mockAPIExclude =
  import.meta.env.VITE_MOCK_API_EXCLUDE &&
  typeof import.meta.env.VITE_MOCK_API_EXCLUDE === 'string' &&
  import.meta.env.MODE !== 'production'
    ? import.meta.env.VITE_MOCK_API_EXCLUDE.split(' ')
    : undefined;

// const copyright = undefined === import.meta.env.VITE_COPYRIGHT ? '' : import.meta.env.VITE_COPYRIGHT;

const storageName = 'state';

const dateSettings = {
  locales: {
    ru: {
      firstDayOfWeek: 2,
      masks: {
        input: 'DD.MM.YYYY',
      },
    },
    en: {
      firstDayOfWeek: 1,
      masks: {
        input: 'DD.MM.YYYY',
      },
    },
  },
};

export const apiConfig = {
  baseUrl: import.meta.env.VITE_API_URL as string,
  tokenStorageName: 'token',
  storageName: 'gis',
};

export const version = `v.${pkg.version}`;

export const mapboxToken: string = (import.meta.env.VITE_MB_TOKEN as string) ?? '';

export default {
  version,
  apiConfig,
  storageName,
  dateSettings,
  mockAPIEnable,
  mockAPIExclude,
  mockAPIInclude,
};
