export class ValidationError extends Error {
  errors: string[];

  constructor(errors: string[], message = 'Validation Failed') {
    super(message);
    this.name = 'Validation Exception';
    this.errors = errors;
  }
}

export default { ValidationError };
