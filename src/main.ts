import { createApp } from 'vue';
import { createPinia } from 'pinia';

import router from './router';
import i18n from './services/i18n';
import App from './App.vue';

import { mockAPIEnable } from './config';
import { startMockAPI } from './mocks/browser';

if (mockAPIEnable) {
  startMockAPI();
}

const pinia = createPinia();

const app = createApp(App);

app.use(i18n);
app.use(router);
app.use(pinia);

app.mount('#app');
