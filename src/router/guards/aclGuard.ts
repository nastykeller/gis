import { useUserStore } from '@/stores/userStore';
import { defineAbilityFor, ACTIONS } from '@/services/acl';

import { RouteLocationNormalized, RouteLocationRaw } from 'vue-router';

const aclGuard = (to: RouteLocationNormalized): RouteLocationRaw | boolean => {
  const store = useUserStore();

  const abilities = defineAbilityFor(store.profile.role);

  if (to.meta.resource) {
    const actions: string[] = (to.meta.action as string[]) ?? [ACTIONS.READ];
    const resource: string = to.meta.resource as string;
    const canNavigate = actions.every(action => abilities.can(action, resource));

    if (!canNavigate) {
      return { name: 'root' };
    }
  }

  return true;
};

export default aclGuard;
