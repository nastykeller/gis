import { useRouterStore } from '@/stores/routerStore';

import { RouteLocationNormalized } from 'vue-router';

export const historyBeforeGuard = (to: RouteLocationNormalized, from: RouteLocationNormalized): void => {
  const routerStore = useRouterStore();
  if (from) {
    routerStore.setPreviousRoute(from);
  }

  if (to.matched.length > 1) {
    routerStore.setParentRoute({ ...to, path: to.matched[0].path, name: to.matched[0].name });
  }
};
