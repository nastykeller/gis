import { useUserStore } from '@/stores/userStore';

import { RouteLocationNormalized, RouteLocationRaw } from 'vue-router';

const authenticationBeforeRouting = (to: RouteLocationNormalized): RouteLocationRaw | boolean => {
  const store = useUserStore();

  const token = localStorage.getItem('token');

  if (!to.meta.public && !token) {
    store.logout();
    return { name: 'signin' };
  }

  return true;
};

export default authenticationBeforeRouting;
