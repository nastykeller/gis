import { useUserStore } from '@/stores/userStore';
import { RouteLocationRaw } from 'vue-router';

const loginEntryGuard = (): RouteLocationRaw | boolean => {
  const store = useUserStore();

  if (store.isAuthenticated) {
    return { name: 'root' };
  }

  return true;
};

export default loginEntryGuard;
