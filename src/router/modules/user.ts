import loginEntryGuard from '../guards/loginEntryGuard';
import { Component } from 'vue';

export default [
  {
    path: '/signin',
    name: 'signin',
    component: (): Promise<Component> => import('@/views/account/SignIn.vue'),
    meta: {
      public: true,
      modal: true,
    },
    beforeEnter: loginEntryGuard,
  },
  {
    path: '/signup',
    name: 'signup',
    component: (): Promise<Component> => import('@/views/account/SignUp.vue'),
    meta: {
      public: true,
      modal: true,
    },
  },
  {
    path: '/restore-password',
    name: 'restore-password',
    component: (): Promise<Component> => import('@/views/account/RestorePassword.vue'),
    meta: {
      public: true,
      modal: true,
    },
  },
  {
    path: '/reset-password',
    name: 'reset-password',
    component: (): Promise<Component> => import('@/views/account/RestorePassword.vue'),
    meta: {
      public: true,
      modal: true,
    },
  },
  {
    path: '/profile',
    name: 'profile',
    component: (): Promise<Component> => import('@/views/account/RestorePassword.vue'),
    meta: {
      public: true,
      modal: true,
    },
  },
  {
    path: '/profile/edit',
    name: 'profile-edit',
    component: (): Promise<Component> => import('@/views/account/RestorePassword.vue'),
    meta: {
      public: true,
      modal: true,
    },
  },
  {
    path: '/profile/reports',
    name: 'profile-reports',
    component: (): Promise<Component> => import('@/views/account/ProfileReportIndex.vue'),
    children: [
      {
        path: ':id',
        name: 'profile-report',
        component: (): Promise<Component> => import('@/views/report/ReportView.vue'),
        props: true,
      },
    ],
  },
];
