import { Component } from 'vue';
import { RouteLocationNormalized } from 'vue-router';

const reportCast = (route: RouteLocationNormalized) => {
  return {
    id: Number.parseInt(route.params?.id as string, 10) ?? null,
  };
};

export default [
  {
    path: '/search/:term?',
    name: 'search',
    component: (): Promise<Component> => import('@/views/report/ReportIndex.vue'),
    children: [
      {
        path: 'report/:id',
        name: 'search-report',
        component: (): Promise<Component> => import('@/views/report/ReportView.vue'),
        props: reportCast,
        meta: {
          public: true,
        },
      },
    ],
    meta: {
      public: true,
    },
  },
  {
    path: '/report/:id',
    name: 'report-view',
    component: (): Promise<Component> => import('@/views/report/ReportView.vue'),
    props: true,
    meta: {
      public: true,
    },
  },
  {
    path: '/report/create',
    name: 'report-create',
    component: (): Promise<Component> => import('@/views/report/ReportEdit.vue'),
    meta: {
      public: true,
    },
  },
  {
    path: '/report/:id/edit',
    name: 'report-edit',
    component: (): Promise<Component> => import('@/views/report/ReportEdit.vue'),
    props: reportCast,
    meta: {
      public: true,
    },
  },
];
