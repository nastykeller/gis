import { createRouter, createWebHistory } from 'vue-router';

import { historyBeforeGuard } from './guards/historyGuard';
import aclGuard from './guards/aclGuard';
import authGuard from './guards/authGuard';

import report from './modules/report';
import user from './modules/user';

const config = {
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      name: 'root',
      component: () => import('@/views/HomeIndex.vue'), // TODO: think can we do it beauty
      meta: {
        public: true,
      },
    },

    {
      path: '/styleguide',
      name: 'styleguide',
      components: { notfound: () => import('@/views/AppStyleGuide.vue') },
      meta: {
        notfound: true,
        public: true,
      },
    },
    ...report,
    ...user,

    {
      path: '/:pathMatch(.*)*',
      components: { notfound: () => import('@/views/404.vue') },
      meta: {
        notfound: true,
        public: true,
      },
    },
  ],
};

const router = createRouter(config);

router.beforeEach(authGuard);
router.beforeEach(aclGuard);
router.beforeEach(historyBeforeGuard);

export default router;
